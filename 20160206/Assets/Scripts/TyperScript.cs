﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//[RequiredComponent(typeof(Text))]

public class TyperScript : MonoBehaviour {

	public string msg = "Replace";
	private Text textComp;
	public float startDelay =2f;
	public float typeDelay = 0.01f;

	// Use this for initialization
	void Start () {
		StartCoroutine ("TypeIn");
	}
	void Awake()
	{
		textComp = GetComponent<Text>();
	}

	public IEnumerator TypeIn()
	{
		yield return new WaitForSeconds(startDelay);

		for (int i = 0; i < msg.Length; i++)
	{
		textComp.text = msg.Substring (0,i);
		yield return new WaitForSeconds (typeDelay);
	}
}
	
public IEnumerator TyperOff()
{
	for (int i = msg.Length; i >=0; i--)
	{
		textComp.text = msg.Substring (0, i);
		yield return new WaitForSeconds(typeDelay);
	}
}
}
