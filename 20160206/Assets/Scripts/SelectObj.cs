﻿using UnityEngine;
using System.Collections;

public class SelectObj : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0 &&
			Input.GetTouch(0).phase == TouchPhase.Began) {
			gameObject.GetComponent<Renderer>().material.color = Color.green;
		}
		if (Input.touchCount > 0 &&
			Input.GetTouch(0).phase == TouchPhase.Ended) {
			gameObject.GetComponent<Renderer>().material.color = new Color (0, 1, 0, 1);
		}
			
	}
}
