// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33389,y:32426,varname:node_9361,prsc:2|custl-2402-OUT,clip-3280-OUT;n:type:ShaderForge.SFN_Tex2d,id:1013,x:32946,y:31959,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_1013,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:03374fee270fc46328f5292ff633de7e,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:3602,x:32501,y:32400,ptovrint:False,ptlb:node_3602,ptin:_node_3602,varname:node_3602,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:fb617173cf6441699a23330870d81a35,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:3566,x:32512,y:32767,ptovrint:False,ptlb:Blend_Amount (OpacityClip),ptin:_Blend_AmountOpacityClip,varname:node_3566,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Vector1,id:1327,x:32556,y:32596,varname:node_1327,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Multiply,id:3280,x:33027,y:32556,varname:node_3280,prsc:2|A-7552-OUT,B-3566-OUT;n:type:ShaderForge.SFN_Add,id:7552,x:32815,y:32347,varname:node_7552,prsc:2|A-3602-R,B-1327-OUT;n:type:ShaderForge.SFN_Color,id:9022,x:32694,y:32064,ptovrint:False,ptlb:ss,ptin:_ss,varname:node_9022,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Slider,id:1099,x:32580,y:32265,ptovrint:False,ptlb:AddColor,ptin:_AddColor,varname:node_1099,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:5;n:type:ShaderForge.SFN_Multiply,id:2402,x:33195,y:32093,varname:node_2402,prsc:2|A-1013-RGB,B-8354-OUT;n:type:ShaderForge.SFN_Add,id:8354,x:33070,y:32259,varname:node_8354,prsc:2|A-9022-RGB,B-1099-OUT;proporder:1013-3602-3566-9022-1099;pass:END;sub:END;*/

Shader "Shader Forge/RoofDiffuceShader" {
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _node_3602 ("node_3602", 2D) = "white" {}
        _Blend_AmountOpacityClip ("Blend_Amount (OpacityClip)", Range(0, 1)) = 1
        _ss ("ss", Color) = (1,1,1,1)
        _AddColor ("AddColor", Range(0, 5)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform sampler2D _node_3602; uniform float4 _node_3602_ST;
            uniform float _Blend_AmountOpacityClip;
            uniform float4 _ss;
            uniform float _AddColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _node_3602_var = tex2D(_node_3602,TRANSFORM_TEX(i.uv0, _node_3602));
                clip(((_node_3602_var.r+0.5)*_Blend_AmountOpacityClip) - 0.5);
////// Lighting:
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                float3 finalColor = (_Texture_var.rgb*(_ss.rgb+_AddColor));
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _node_3602; uniform float4 _node_3602_ST;
            uniform float _Blend_AmountOpacityClip;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _node_3602_var = tex2D(_node_3602,TRANSFORM_TEX(i.uv0, _node_3602));
                clip(((_node_3602_var.r+0.5)*_Blend_AmountOpacityClip) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
